/*
Package mdicon makes available Google's Material Design icon set in SVG string
format.
*/
package mdicon

//go:generate go run generate/generate.go

const (
	// MinimalAttributes applied to the svg tag for the SVG icons in this
	// package: all icons will use at least these attributes.
	MinimalAttributes = " " + "xmlns=" + XMLNamespace + " " +
		"viewBox=" + ViewBox + " "

	// ViewBox is the quoted value of the viewBox attribute for the svg tag for
	// the SVG icons in this package.
	ViewBox = "\"0 0 24 24\""

	// XMLNamespace is the quoted value of the xmlns attribute for an SVG
	// element.
	XMLNamespace = "\"http://www.w3.org/2000/svg\""
)
