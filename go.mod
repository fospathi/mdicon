module gitlab.com/fospathi/mdicon

go 1.19

require gitlab.com/fospathi/universal v0.0.0-20230129201124-cb8e6e74c4c8

require github.com/gorilla/websocket v1.5.0 // indirect

replace gitlab.com/fospathi/universal => ../universal
